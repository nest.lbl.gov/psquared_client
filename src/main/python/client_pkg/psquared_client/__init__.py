"""
This module provides the classes and functions that collaborate to
create a Command Line Interface for **PSquare** clients.
"""

from . import display
from .psquared import PSquared
from .request_error import RequestError
from .response_error import ResponseError
